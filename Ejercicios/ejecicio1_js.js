var avengers = ['Spiderman', 'AntMan', 'Hulk'];
var mutants = ['Wolverine', 'Ciclops', 'Storm'];
var defenders = ['Daredevil', 'IronFist', 'Luke Cage'];

var calcListLength = (list) => {
    var myLength = 0;
    list.forEach(element => {
        myLength = myLength + element.length;
    });
    return myLength;
}
var avengersLength = calcListLength(avengers);
var mutantsLength = calcListLength(mutants);
var defendersLength = calcListLength(defenders);

var comparator = (a,b,c) => {
    if (a > b) {
        if (a == c)
          return 'Empate entre a y c';
        if (a > c)
          return 'a gana';
        else
          return 'c gana';
      } else {
        if (a == b)
          return 'Empate entre a y b';
        if (b > c)
          return 'b gana';
        else
          return 'c gana';
      }
}
console.log(comparator(avengersLength,mutantsLength, defendersLength));

var media = (a,b,c) => {
    return a+b+c / 3
}
console.log(media(avengersLength,mutantsLength, defendersLength));

/* 
* Ejercicio 1
* → Devuelve el string de mayor longitud
*/

// Declaramos un array -> Cambiar nombre de myArray al vuestro e introducimos valores
var myArray = [];
// Declaramos una variable donde guardaremos el string mayor de nuestro myArray
var longestElement= '';
// PRIMERA ITERACIÓN: Recorrer el Array y comprobar la longitud de cada uno de ellos.
myArray.forEach( (element) => {
    // SEGUNDA ITERACIÓN: Si longestElement es mayor a relement cambiamos el valor de longestElement
    if() {
        longestElement = element
    }
});
// TERCERA ITERACIÓN MOSTRAR EL ELEMENTO MAYOR
console.log(longestElement);